import scipy
from scipy import array


def lbp_patch(patch):
    """
    :param patch: LBP の 3x3 探索パッチ
    :return: LBP変換後の値
    >>> j=array([[4,15,12],[16,15,14],[13,8,16]])
    >>> lbp_patch(j)
    9
    """
    k = scipy.zeros((3,3), str)
    for a in range(3):
        for b in range(3):
            if patch[a,b]>patch[1,1]:
                k[a,b]="1"
            else:
                k[a,b]="0"
    pattern = k[0,0]+k[0,1]+k[0,2]+k[1,2]+k[2,2]+k[2,1]+k[2,0]+k[1,0]
    return int(pattern,2)


def lbp(image):
    """
    :param image:
    :return:
    >>> j=array([[4,15,12],[16,15,14],[13,8,16]])
    >>> lbp(j)
    array([[9]])
    """
    (h, w) = image.shape
    lbp_image = scipy.empty((h-2, w-2), int)
    for y in range(lbp_image.shape[0]):
        for x in range(lbp_image.shape[1]):
            patch = image[x:x+3, y:y+3]
            lbp_image[x,y] = lbp_patch(patch)
    return lbp_image


if __name__ == "__main__":
    import doctest
    doctest.testmod()